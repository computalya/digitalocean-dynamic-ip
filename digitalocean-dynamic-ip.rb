#!/usr/bin/env ruby
# filename : digitalocean-dynamic-ip.rb

# requirements
require 'JSON'
require 'droplet_kit'

# variables
config_file = '.digitalocean-dynamic-ip.config'

# main
# read access_key
file = File.read(config_file)
ACCESS_TOKEN = JSON.parse(file)['access_token']
DOMAIN = JSON.parse(file)['domain']
SUB_DOMAIN = JSON.parse(file)['sub_domain']


client = DropletKit::Client.new(access_token: ACCESS_TOKEN)
# x = client.domains.find(name: DOMAIN)
# client.domain_records.update(domain_record, for_domain: DOMAIN, id: 'id')
