# README

Using official [DropletKit](https://github.com/digitalocean/droplet_kit) from DigitalOcean to update the dynamic IP address

# Requirements

## Install gem for ruby

```bash
gem install droplet_kit
```

## DigitalOcean Access Token

1. Login to your [DigitalOcean](https://www.digitalocean.com) account
2. select **API**
3. select **Tokens/Keys**
4. **Generate New Token** with `read & write` access

## create a config file on your machine

```bash
touch $HOME/.digitalocean-dynamic-ip.config
```

copy lines below and add your token

```json
{
  "access_token": "your-token-here",
  "domain": "example.com",
  "sub_domain": "sub"
}
```

## Installation

# TO DO

- add installation to README
- add screenshot of DigitalOcean Networks configuration
- add screenshot from the final script running in the console 
- add crontab example
